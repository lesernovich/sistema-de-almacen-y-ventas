package domain;

public class Raton extends DispositivosEntrada{

    private final int idRaton;
    private static int contadorRatones;
    
    public Raton(String entrada, String marca){
   
        super(entrada, marca);
        this.idRaton=++contadorRatones;
    }

    @Override
    public String toString() {
        return "Raton[" + "idRaton=" + idRaton +","+ super.toString() +']';
    }
    
}
