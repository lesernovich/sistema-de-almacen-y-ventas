package pc;

import domain.*;

public class Prueba {
    public static void main(String[] args) {
        
        Monitor monitorHP = new Monitor("HP", 38);
        Teclado tecladoLogitech = new Teclado("USB", "Logitech");
        Raton ratongenerico = new Raton("USB", "tianguis");
        
        Computadora compuGamer = new Computadora("Conqueror", monitorHP, tecladoLogitech, ratongenerico);
        
        Monitor monitorCuadrado = new Monitor("QH", 20);
        Teclado tecladoCoolerMaster = new Teclado("USB", "CoolerMaster");
        Raton ratongamer = new Raton("USB", "Logitech");
        
        Computadora compuMasGamer = new Computadora("Aurora", monitorHP, tecladoLogitech, ratongamer);
        
        Orden orden = new Orden();
        orden.agragarComputadora(compuMasGamer);
        orden.agragarComputadora(compuMasGamer);
        orden.agragarComputadora(compuGamer);
        orden.mostrarOrden();
    }
}
